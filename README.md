## Requirements
php version >= 7.4.8
symfony binary
composer
## Steps : 
1. nav to project dir : `cd test_re_sf`
2. install the decencies :  `composer install`
2. create db : `php bin/console doctrine:schema:create` or `php bin/console make:mirgration`
3. load fixtures : `php bin/console doctrine:fixtures:load`
4. start local server : `symfony server:start`
5. mailer catcher : visit [https://mailtrap.io/][mailtrap] connect to google account with : email `tracking.sites.symfony@gmail.com`, password : `tsites1!`
6. to update sites status run the command: `php bin/console check-sites-status` to check the real status for given sites; use the `--force-ko` to force ko status for all sites
[mailtrap]: https://mailtrap.io/